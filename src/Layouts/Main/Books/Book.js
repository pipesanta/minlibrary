import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const useStyles = makeStyles((theme) => ({
    root: {
        border: "1px solid grey",
        borderRadius: "20px",
        marginTop: "12px",
        marginBottom: "12px",
        padding: "10px",
        flexGrow: 1,

        "& > .MuiButtonBase-root" : {
            padding: "0px",
            border: "0px",
            // borderRadius: "15px"
        },
    },
    name: {
        // padding: theme.spacing(1),
        textAlign: 'center',
        fontSize: "20px",
        color: theme.palette.text.primary,
    },
    author: {
        // padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    tag:{
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }

  }));

const BOOK_STATE_MAPPER = {
    GOOD : "Bueno",
    BAD: "Malo",
    REGULAR: "Regular"
}

export default function BookComponent(props){
    const { name, author, onBorrow, onEdit, onDelete, location, state, borrow } = props;
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);

    return (
        <Paper className={classes.root} >
            <Grid container spacing={1}>
                <Grid item xs={11}>
                    <div className={classes.name}>{name}</div>
                </Grid>
                <Grid item xs={1}>
                    <div style={{padding: "0px"}}>
                    <IconButton style={{padding: "0px"}}
                        aria-label="more"
                        aria-controls="long-menu"
                        aria-haspopup="true"
                        onClick={(e) => setAnchorEl(e.currentTarget)}
                    >
                        <MoreVertIcon />
                    </IconButton>
                    </div>
                </Grid>
                <Grid item xs={11}>
                    <div className={classes.author}>{author}</div>
                </Grid>
                <Grid item xs={4}>
                    <div className={classes.tag}>{location}</div>
                </Grid>
                <Grid item xs={4}>
                    <div className={classes.tag} >{BOOK_STATE_MAPPER[state]}</div>
                </Grid>
                <Grid item xs={4}>
                    <div className={classes.tag} >{borrow === null ? "Disponible": "Prestado"}</div>
                </Grid>
            </Grid>

            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={() => setAnchorEl(null)}
            >
                <MenuItem onClick={() => { setAnchorEl(null); onEdit(); }}> Editar </MenuItem>
                <MenuItem onClick={() => { setAnchorEl(null); onBorrow(); }}> Prestar </MenuItem>
                <MenuItem onClick={() => { setAnchorEl(null); onDelete(); }}> Borrar  </MenuItem>
                {/* <MenuItem onClick={handleClose}>Logout</MenuItem> */}
            </Menu>
        </Paper>
    )
}