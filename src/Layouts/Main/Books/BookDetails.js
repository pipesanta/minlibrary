import React, {useState, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import ApiRest from '../../../Services/apiRest';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles((theme) => ({
    root: {
        margin: "20px",
        '& .MuiTextField-root': {
            // margin: theme.spacing(1),
            width: '100%',
        },
        "& > .textField": {
            // background: "silver",
            paddingTop: "5px",
            paddingBottom: "5px",
        }
    },
    formControl: {
        width: '100%',
        // margin: theme.spacing(1),
        // minWidth: 120,1
    },

  }));

  const EMPTY_FORM = { code: "", title: "", author: "", state: "GOOD", location:"", comments: ""};

export default function Books(props){

    const { match, location } = props;
    const classes = useStyles();
    const { enqueueSnackbar } = useSnackbar();
    const [ bookId, setBookId ] = useState(null);
    const [ apiService ] = useState(ApiRest());
    const [formValue, setFormValue] = useState(EMPTY_FORM);
    const [ formIsValid, setFormIsValid ] = useState(false);

    useEffect(() =>{
        const queryParams = match.params;
        const newBookId = (queryParams.id === "new")
            ? null
            : queryParams.id;

        setBookId(newBookId);

        if(newBookId){
            apiService.get("/books/findOneById", {id: newBookId})
                .then(result => {
                    setFormValue(result.data)
                })
                .catch(e => {
                    console.log(e);
                });
        }

    }, []);

    useEffect(() => {
        const route = location.pathname;
        if( route === "/main/books/new" &&  bookId === null){
            setFormValue(EMPTY_FORM);
        }
      }, [location]);

    const onFormChange = (field, event) => {
        const value = event.target.value;
        const newFormValue = {...formValue};
        newFormValue[field] = value;

        setFormValue(newFormValue);

        // validate form
        if(!formValue.title || !formValue.author || !formValue.location ){
            setFormIsValid(false)
        }else{
            setFormIsValid(true);
        }
    }

    const onCreateOrUpdateBook = () => {
        const snackBarOptions = { 
            variant: 'success',
            transitionDuration: 600,
            autoHideDuration: 5000
        };
        const path = bookId ? "/books/updateOneById" : "/books/insertOne";
        const params = bookId ? { ...formValue, id: bookId } : formValue; 

        apiService.post( path , params)
            .then(response => {
                console.log(response.data);
                const msj = `Libro ${ bookId ? "Actualizado" : "Creado" } Exitosamente`;
                enqueueSnackbar(msj, snackBarOptions );
                if(!bookId){
                    setFormValue(EMPTY_FORM);
                }
            })
            .catch(err => {
                const msj = `No se ha podido ${ bookId ? "Actualizar" : "Crear" } el libro`
                enqueueSnackbar(msj, {...snackBarOptions, variant: "error"} );
            });

        
    }



    return (
        <div className={classes.root}>
           
            <div style={ { fontSize:"25px", textAlign: "center" } } > 
                { bookId ? "Detalles del libro": "Crear el libro" }
            </div>
            <div className="textField" >
                <TextField value={formValue.code} label="Código"
                    onChange={onFormChange.bind(this, "code")}
                    type="text" variant="outlined"
                    InputProps ={{ autoComplete: "off"}}
                />
            </div>
            
            <div className="textField" >
                <TextField value={formValue.title} label="Nombre" required
                    onChange={onFormChange.bind(this, "title")}
                    type="text" variant="outlined"
                    InputProps ={{ autoComplete: "off"}}
                />
            </div>
            
            <div className="textField" >
                <TextField value={formValue.author} label="Autor" required
                    onChange={onFormChange.bind(this, "author")}
                    type="text" variant="outlined"
                    InputProps ={{ autoComplete: "off"}}
                />
            </div>

            <div className="textField" >
                <TextField value={formValue.location} label="Ubicación" required
                    onChange={onFormChange.bind(this, "location")}
                    type="text" variant="outlined"
                    InputProps ={{ autoComplete: "off"}}
                />
            </div>
            
            <div className="textField" >
                <FormControl variant="outlined" className={classes.formControl}>
                    <InputLabel>Estado</InputLabel>
                    <Select
                        value={formValue.state}
                        onChange={onFormChange.bind(this, "state")}
                        label="Estado"
                    >
                    {/* <MenuItem value=""> <em>None</em> </MenuItem> */}
                    <MenuItem value={"GOOD"}>Bueno</MenuItem>
                    <MenuItem value={"BAD"}>Malo</MenuItem>
                    <MenuItem value={"REGULAR"}>Regular</MenuItem>
                    </Select>
                </FormControl>
            </div>
            
            <div className="textField" >
                <TextField
                    label="Comentarios" multiline
                    rows={4}  value={formValue.comments}
                    onChange={onFormChange.bind(this, "comments")}
                    variant="outlined"
                />
            </div>

            <div className="textField" >
                <Button variant="contained" color="primary" disabled={!formIsValid} onClick={onCreateOrUpdateBook}>
                    { (bookId) ?  "Actualizar Libro" : "Crear Libro" }
                </Button>
            </div>

            
            
            
        </div>
    )
}
