import React, { useState, useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import BookComponent from './Book';
import APIRest from '../../../Services/apiRest';

import { Subject, defer, of, BehaviorSubject, merge } from 'rxjs';
import { debounceTime, mergeMap, map } from 'rxjs/operators';
import { useObservable } from 'rxjs-hooks';

import { useSnackbar } from 'notistack';
// components
import DialogConfirmation from '../../../components/ConfirmationDialog';

import { useHistory } from 'react-router-dom';

const onSearch$ = new Subject().pipe(
    debounceTime(300)
);
const listUpdates$ = new BehaviorSubject([]);

const useStyles = makeStyles((theme) => ({
    root: {
        margin: "20px",
        '& .MuiTextField-root': {
            // margin: theme.spacing(1),
            width: '100%',
        },
        "& > .bookResultDiv": {
            // background: "silver",
            paddingTop: "10px",
            paddingBottom: "10px",
        }
    }

  }));

const ALEJITA= [
    { 
        id: 0, title: "Alejita", author: "Te Quiero mucho", 
        location: ":D", state: ":P", borrow: { cc: "algo" }
    }
];

export default function SearchComponent(props){
    const classes = useStyles();
    const history = useHistory();

    const { enqueueSnackbar } = useSnackbar();
    const [ ApiService ] = useState(APIRest()); 

    const [ filterText, setFilterText ] = useState("");
    const [dialogConfirmationOpen, setDialogConfirmationOpen] = useState(false);
    const [ bookIdToDelete, setBookIdToDelete ] = useState(null);

    const resultList = useObservable(() => merge(
        listUpdates$,
        onSearch$.pipe(
            mergeMap(value => value.toUpperCase() !== "ALEJITA"
                ? defer(() => ApiService.get("/books/search", { filter: value }))
                : of({ data: ALEJITA })
            ),
            map(response => response.data)
        ),
        

    ));

    useEffect(() => { onSearch$.next(""); }, []);

    function handleBorrowBook(){

    }

    function handleEdit(bookId){
        history.push(`/main/books/${bookId}`);
    }

    function handleDeleteBook(bookId){        
        setBookIdToDelete(bookId);
        setDialogConfirmationOpen(true);
    }

    function onCloseDialogConfirmation(userConfirmation){

        const bookToDelete = resultList.find(b => b._id === bookIdToDelete);
        const newResultList = resultList.filter(i => i._id !== bookIdToDelete);

        const snackBarOptions = { 
            variant: 'success',
            transitionDuration: 600,
            autoHideDuration: 5000
        };

        if(bookToDelete.borrow && userConfirmation){
            enqueueSnackbar(`${bookToDelete.title} NO SE PUEDE BORRAR, porque está prestado`,
                {...snackBarOptions, variant: "error"}
            );
            setDialogConfirmationOpen(false);
            return
        }

        if(userConfirmation){
            ApiService.post("/books/deleteOne", { id: bookIdToDelete })
                .then(result => {
                    enqueueSnackbar(`${bookToDelete.title} borrado con éxito`, snackBarOptions );
                    listUpdates$.next(newResultList);
                })
                .catch(e => {
                    console.log(e);
                    enqueueSnackbar(`${bookToDelete.title} borrado con éxito`, {...snackBarOptions, variant: "error"} );
                });
        }

        setDialogConfirmationOpen(false);
    }

    function onFilterTextChanged(e){
        const value = e.target.value;
        setFilterText(value);
        onSearch$.next(value);
    }

    return (
        <div className={classes.root}>
            <div>
                <TextField id="filled-search" value={filterText} label="Búsqueda Rápida"
                    onChange={onFilterTextChanged}
                    type="search" variant="outlined"
                    InputProps ={{ autoComplete: "off"}}
                />
            </div>       
            
            <div className="bookResultDiv"> {
                (resultList||[]).map((book, i) => <BookComponent
                    key={i} location={book.location} state={book.state} borrow={book.borrow}
                    name={book.title}
                    author={book.author}
                    onEdit= { handleEdit.bind(this, book._id) }
                    onBorrow={ handleBorrowBook.bind(this, book._id) }
                    onDelete={ handleDeleteBook.bind(this, book._id) }
                />)
            } 
            </div>

            <DialogConfirmation 
                open={dialogConfirmationOpen}
                handleClose={onCloseDialogConfirmation}
                textBody={`NOTA: Los libros prestados no se pueden eliminar`} 
                textTitle="¿Estás segur@ de eliminar este libro ?" >
            </DialogConfirmation>
        </div>
    )
}