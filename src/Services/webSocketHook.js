import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
let instance = null;

const formatMessage = (data) => {
  try {
    const parsed = JSON.parse(data);
    return parsed;
  } catch (err) {
    return null;
  }
};

class WebSocketService{

  constructor(url, retries= 300, retryInterval = 1500){
    this.url = url;
    this.retries =  retries;
    this.retryInterval =  retryInterval;
    this.incomingMessages$ =  new BehaviorSubject(null);
    this.connect();

  }

  connect(){
    console.log("trying to connect to WS server , retries", this.retries);
    
    this.retries--;
    this.wsClient = new WebSocket(this.url);

    this.wsClient.onopen = () => {
      console.log('Connected to socket');

      this.wsClient.onmessage = (event) => {  
        const msg = formatMessage(event.data);
        if(msg){
          this.incomingMessages$.next(msg);
        }
        
      };

    };

    this.wsClient.onclose = () => {
      console.log("closing ws connection");
      if(this.retries > 0){
        setTimeout(() => this.connect(), this.retryInterval );
      }
     
    };

  }

  listenMessages$(topic){
    return this.incomingMessages$.pipe(
      filter(msg => msg != null)
    )
  }

}


/**@returns {WebSocketService} */
export default () => {
  if(!instance){
      const host = process.env.API_HOST;
      const port = process.env.API_HOST_PORT;
      return new WebSocketService( `ws://${host}:${port}` );

  }
  return instance;
}